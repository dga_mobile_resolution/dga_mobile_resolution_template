import IndexTemplate from './components/template';
import {BrannerComponent} from 'dba_mobile_branner'
import Grid from "@material-ui/core/Box";
function App() {
  return (
    <div>
      <IndexTemplate />
      <Grid item xs={12} pt={4}>
        <BrannerComponent />
      </Grid>
     
    </div>
  );
}

export default App;
